import { useEffect, useState } from "react";
import styled from "styled-components"
import { popularProducts } from "../data";
import Product from "./Product";
import axios from "axios";

const Container = styled.div`
    height: 100vh;
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    padding: 10px;
`;

const Products = ({cat,filters,sort}) => {
    
    const [products, setProducts] = useState([]);
    const [filteredProducts, setFilteredProducts] = useState([]);

    useEffect(() => {
        const getProducts = async () => {
            try {
                const res = await axios.get( cat ? `http://localhost:3000/api/products?category=${cat}` 
                : "http://localhost:3000/api/products" );
                setProducts(res.data);
            } catch (error) {
                console.log(error);
            }
        };
        getProducts();
    }, [cat]);

    useEffect(() => {
        cat && setFilteredProducts(
            products.filter((item) => 
                Object.entries(filters).every(([key, value]) => 
                    item[key].includes(value)
                )
            )
        );
    }, [products, cat, filters]);

    return (
        <Container>
            {filteredProducts.map((item) => (
                <Product item={item} key={item.id} />
            ))}
        </Container>
    )
}

export default Products
