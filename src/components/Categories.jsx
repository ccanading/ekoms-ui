import styled from "styled-components"
import { categories } from "../data"
import CategoryItem from "./CategoryItem";

const Container = styled.div`  
    display: flex;
    padding: 20px;
    height: 100vh;
    align-items: center;
    justify-content: space-between;
    background-color: #DADADA;
`;

const Categories = () => {
    return (
        <Container>
            {categories.map(item=>(
                <CategoryItem item={item} key={item.id} />
            ))}
        </Container>
    )
}

export default Categories
