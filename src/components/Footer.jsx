import { EmailOutlined, Facebook, Instagram, LocationOn, PhoneAndroid, Pinterest, Twitter } from "@material-ui/icons";
import styled from "styled-components"

const Container = styled.div`
    display: flex;
    background-color: teal;
`;
const Left = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 20px;
`;

const Logo = styled.h1`
    font-size: 100px;
    text-decoration: 10px underline;
`;

const Desc = styled.p`
    margin: 20px 0px;
`;

const SocialContainer = styled.div`
    display: flex;
`;

const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: white;
    background-color: #${props=>props.color};
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 20px;
`;

const Center = styled.div`
    flex: 1;
    padding: 20px;
`;

const Title = styled.h3`
    margin-bottom: 30px;
`;

const List = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    flex-wrap: wrap;
`;

const ListItem = styled.li`
    width: 50%;
    margin-bottom: 10px;
`;

const Right = styled.div`
    flex: 1;
    padding: 20px;
`;

const ContactItem = styled.div`
    margin-bottom: 20px;
    display: flex;
    align-items: center;
`;

const Payment = styled.img`
    width: 50%;
`;


const Footer = () => {
    return (
        <Container>
            <Left>
                <Logo>EKOMS.</Logo>
                <Desc>Where there is Smoke, there is fire. Lit up your Fashion and get served with the Hottest trend in town.</Desc>
                <SocialContainer>
                    <SocialIcon color ="3B5999">
                        <Facebook/>
                    </SocialIcon>
                    <SocialIcon color ="E4405F">
                        <Instagram/>
                    </SocialIcon>
                    <SocialIcon color ="55ACEE">
                        <Twitter/>
                    </SocialIcon>
                    <SocialIcon color ="E60023">
                        <Pinterest/>
                    </SocialIcon>
                </SocialContainer>
            </Left>
            <Center>
                <Title>Useful Links</Title>
                <List>
                    <ListItem>Home</ListItem>
                    <ListItem>Cart</ListItem>
                    <ListItem>Men's Fashion</ListItem>
                    <ListItem>Women's Fashion</ListItem>
                    <ListItem>Accessories</ListItem>
                    <ListItem>Popular</ListItem>
                    <ListItem>My Account</ListItem>
                    <ListItem>Order Tracking</ListItem>
                    <ListItem>Wishlist</ListItem>
                    <ListItem>Terms</ListItem>
                </List>
            </Center>
            <Right>
                <Title>Contact</Title>
                <ContactItem><LocationOn style={{marginRight:"10px"}} /> Kalayaan, Makati 1214</ContactItem>
                <ContactItem><PhoneAndroid style={{marginRight:"10px"}}/> +639 927 765 1234</ContactItem>
                <ContactItem><EmailOutlined style={{marginRight:"10px"}}/>webhaven.solutions@gmail.com</ContactItem>
                <Payment src="https://user-images.githubusercontent.com/52581/44384465-5e312780-a570-11e8-9336-7b54978a9e64.png"/>
            </Right>
        </Container>
    )
}

export default Footer
